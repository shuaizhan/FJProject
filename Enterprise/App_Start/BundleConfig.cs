﻿using System.Web;
using System.Web.Optimization;

namespace Enterprise
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            //前台js
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                      "~/Scripts/jquery.min.js",
                      "~/Scripts/amazeui.min.js"));
            //前台样式
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/amazeui.css",
                      "~/Content/style.css",
                       "~/Content/site.css"));

            //AdminLTE 后台样式
            bundles.Add(new StyleBundle("~/AdminLTE/admincss").Include(
                        "~/AdminLTE/bootstrap/css/bootstrap.min.css",
                        "~/AdminLTE/bootstrap/css/font-awesome.min.css",
                        "~/AdminLTE/bootstrap/css/ionicons.min.css",
                        "~/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css",
                        "~/AdminLTE/dist/css/AdminLTE.min.css",
                        "~/AdminLTE/dist/css/skins/_all-skins.min.css"));
            //AdminLTE 后台js
            bundles.Add(new ScriptBundle("~/AdminLTE/adminjs").Include(
                        "~/AdminLTE/plugins/jQuery/jQuery-2.2.0.min.js",
                        "~/AdminLTE/bootstrap/js/bootstrap.min.js",
                        "~/AdminLTE/plugins/fastclick/fastclick.js",
                        "~/AdminLTE/dist/js/app.min.js",
                        "~/AdminLTE/plugins/sparkline/jquery.sparkline.min.js",
                        "~/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                        "~/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                        "~/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js",
                        "~/AdminLTE/plugins/chartjs/Chart.min.js",
                        "~/AdminLTE/dist/js/pages/dashboard2.js",
                        "~/AdminLTE/dist/js/demo.js"));

            //UEditor
            bundles.Add(new ScriptBundle("~/UEditor/uejs").Include(
                "~/UEditor/ueditor.config.js",
                "~/UEditor/ueditor.all.min.js",
                "~/UEditor/lang/zh-cn/zh-cn.js"));

            //My97DatePicker  WdatePicker
            bundles.Add(new ScriptBundle("~/WdatePicker/wpjs").Include(
                "~/My97DatePicker/WdatePicker.js"));

            //WebUploader
            bundles.Add(new ScriptBundle("~/UEditor/webuploaderjs").Include(
                        "~/AdminLTE/plugins/jQuery/jQuery-2.2.0.min.js",
                        "~/UEditor/third-party/webuploader/webuploader.js",
                        "~/UEditor/third-party/webuploader/upload.js"));
            bundles.Add(new StyleBundle("~/UEditor/webuploadercss").Include(
                      "~/UEditor/third-party/webuploader/webuploader.css",
                      "~/UEditor/third-party/webuploader/style.css"));
        }
    }
}
