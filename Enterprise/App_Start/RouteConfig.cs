﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Enterprise
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "adminLogin",
                url: "admin/login",
                defaults: new { controller = "Account", action = "Login" }
                );

            routes.MapRoute(
                name: "admin",
                url: "admin/{action}",
                defaults: new { controller = "Admin", action = "Index" }
                );

            routes.MapRoute(
                name: "home",
                url: "home/{action}/{callIndex}",
                defaults: new { controller = "Home"},
                constraints: new { action = @"List|ImageList|RecruitList", callIndex = @"^[A-Za-z0-9]+$" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
