﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Enterprise.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    [MetadataType(typeof(MyUser))]
    public class ApplicationUser : IdentityUser
    {
        [DisplayName("真实姓名")]
        [MaxLength(10)]
        public string RealName { get; set; }

        [DisplayName("备注")]
        [MaxLength(128)]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [DisplayName("头像")]
        [DataType(DataType.ImageUrl)]
        public string Pic { get; set; }

        private class MyUser
        {
            [DisplayName("邮箱")]
            public string Email { get; set; }

            [DisplayName("手机号码")]
            public string PhoneNumber { get; set; }

            [DisplayName("用户名")]
            public string UserName { get; set; }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }


    [MetadataType(typeof(MyRole))]
    public partial class Role : IdentityRole
    {
        public Role() : base() { }

        public Role(string name) : base(name) { }

        [DisplayName("中文名称")]
        public string ChnName { get; set; }

        private class MyRole
        {
            [DisplayName("角色名称")]
            public string Name { get; set; }
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false) 
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleCategory> ArticleCategories { get; set; }
        public DbSet<Role> IdentityRoles { get; set; }
        public DbSet<FriendLink> FriendLinks { get; set; }
    }
}