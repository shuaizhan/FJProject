﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Enterprise.Models
{
    public class HomeModel
    {
        public BaseInfo BaseInfo { get; set; }
        public IQueryable<Article> Products { get; set; }   //产品展示
        public Article TopNews { get; set; }                //新闻头条
        public IQueryable<Article> News { get; set; }       //新闻
        public IQueryable<Article> Cases { get; set; }      //案例展示
        public IQueryable<FriendLink> FriendLinks { get; set; } //友情链接
        public Article AboutUs { get; set; }    //关于我们
        public Article ContactUs { get; set; }    //联系我们
    }

    public class DetailModel
    {
        public BaseInfo BaseInfo { get; set; }
        public Article Article { get; set; }
        public Article PreArticle { get; set; }
        public Article NextArticle { get; set; }
    }

    public class ListModel
    {
        [Display(Name = "网站信息")]
        public BaseInfo BaseInfo { get; set; }
        [Display(Name = "文章列表")]
        public IList<Article> Articles { get; set; }
    }

    public class FeedbackModel
    {
        public BaseInfo BaseInfo { get; set; }
        public Feedback Feedback { get; set; }
    }
}
