﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Enterprise.Models
{
    public class FriendLink
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "请填写标题")]
        [DisplayName("标题")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "字符长度只能在2-100之间")]
        public string Title { get; set; }

        [Required(ErrorMessage = "请填写链接地址")]
        [DisplayName("链接地址")]
        public string Site_url { get; set; }

        [DataType(DataType.ImageUrl)]
        [DisplayName("图片")]
        public string Img_url { get; set; }

        [DisplayName("排序")]
        public int? Sort_id { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        [DisplayName("添加时间")]
        public DateTime? Add_time { get; set; }
    }
}