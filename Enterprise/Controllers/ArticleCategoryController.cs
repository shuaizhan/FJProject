﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enterprise.Models;

namespace Enterprise.Controllers
{
    [Authorize]
    public class ArticleCategoryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ArticleCategory
        public ActionResult Index()
        {
            return View(db.ArticleCategories.ToList());
        }

        // GET: ArticleCategory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleCategory articleCategory = db.ArticleCategories.Find(id);
            if (articleCategory == null)
            {
                return HttpNotFound();
            }
            return View(articleCategory);
        }

        // GET: ArticleCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ArticleCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Call_index,Seo_title,Seo_keyword,Seo_description,Description")] ArticleCategory articleCategory)
        {
            if (ModelState.IsValid)
            {
                db.ArticleCategories.Add(articleCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(articleCategory);
        }

        // GET: ArticleCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleCategory articleCategory = db.ArticleCategories.Find(id);
            if (articleCategory == null)
            {
                return HttpNotFound();
            }
            return View(articleCategory);
        }

        // POST: ArticleCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Call_index,Seo_title,Seo_keyword,Seo_description,Description")] ArticleCategory articleCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(articleCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(articleCategory);
        }

        // GET: ArticleCategory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleCategory articleCategory = db.ArticleCategories.Find(id);
            if (articleCategory == null)
            {
                return HttpNotFound();
            }
            return View(articleCategory);
        }

        // POST: ArticleCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ArticleCategory articleCategory = db.ArticleCategories.Find(id);
            db.ArticleCategories.Remove(articleCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

// 
