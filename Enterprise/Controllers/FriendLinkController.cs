﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enterprise.Models;
using Webdiyer.WebControls.Mvc;

namespace Enterprise.Controllers
{
    [Authorize]
    public class FriendLinkController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: FriendLink
        public ActionResult Index(int pageIndex = 1, string title = null)
        {
            var query = db.FriendLinks.AsQueryable();
            if (!string.IsNullOrWhiteSpace(title))
                query = query.Where(a => a.Title.Contains(title));
            var model = query.OrderByDescending(x => x.Sort_id).ToPagedList(pageIndex, 10);
            return View(model);
        }

        // GET: FriendLink/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FriendLink friendLinkModels = db.FriendLinks.Find(id);
            if (friendLinkModels == null)
            {
                return HttpNotFound();
            }
            return View(friendLinkModels);
        }

        // GET: FriendLink/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FriendLink/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Site_url,Img_url,Sort_id")] FriendLink friendLinkModels)
        {
            if (ModelState.IsValid)
            {
                if (friendLinkModels.Sort_id == null) friendLinkModels.Sort_id = 0;
                friendLinkModels.Add_time = DateTime.Now;
                db.FriendLinks.Add(friendLinkModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(friendLinkModels);
        }

        // GET: FriendLink/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FriendLink friendLinkModels = db.FriendLinks.Find(id);
            if (friendLinkModels == null)
            {
                return HttpNotFound();
            }
            return View(friendLinkModels);
        }

        // POST: FriendLink/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Site_url,Img_url,Sort_id,Add_time")] FriendLink friendLinkModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(friendLinkModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(friendLinkModels);
        }

        // GET: FriendLink/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FriendLink friendLinkModels = db.FriendLinks.Find(id);
            if (friendLinkModels == null)
            {
                return HttpNotFound();
            }
            return View(friendLinkModels);
        }

        // POST: FriendLink/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FriendLink friendLinkModels = db.FriendLinks.Find(id);
            db.FriendLinks.Remove(friendLinkModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
