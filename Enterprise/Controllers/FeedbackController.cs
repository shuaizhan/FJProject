﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enterprise.Models;
using Webdiyer.WebControls.Mvc;
using System.Xml.Linq;

namespace Enterprise.Controllers
{

    public class FeedbackController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Feedback
        [Authorize]
        public ActionResult Index(int pageIndex = 1, bool isNew = false)
        {
            var query = db.Feedbacks.AsQueryable();
            if (isNew)
                query = query.Where(x => x.Replay_time == null);
            var model = query.OrderByDescending(x => x.Id).ToPagedList(pageIndex, 10);
            return View(model);
        }

        // GET: Feedback/Create
        public ActionResult Create()
        {
            FeedbackModel model = new FeedbackModel();
            model.BaseInfo = GetSiteInfo();
            return View(model);
        }

        // POST: Feedback/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Content,Name,Tel,QQ,Email,Add_time,Replay_content,Replay_time")] Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                if (feedback.Add_time == null)
                {
                    feedback.Add_time = DateTime.Now;
                }
                if (feedback.Name == null)
                {
                    feedback.Name = "匿名";
                }
                db.Feedbacks.Add(feedback);
                db.SaveChanges();
                TempData["message"] = "您已经成功提交留言 [" + feedback.Title + "]";
                return RedirectToAction("Create", "Feedback");
            }

            return View(feedback);
        }

        // GET: Feedback/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Feedback feedback = db.Feedbacks.Find(id);
            if (feedback == null)
            {
                return HttpNotFound();
            }
            return View(feedback);
        }

        // POST: Feedback/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id,Title,Content,Name,Tel,QQ,Email,Add_Time,Replay_content,Replay_time")] Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                if(!String.IsNullOrEmpty(feedback.Replay_content) && feedback.Replay_time==null)
                {
                    feedback.Replay_time = DateTime.Now;
                }
                db.Entry(feedback).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(feedback);
        }

        // GET: Feedback/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Feedback feedback = db.Feedbacks.Find(id);
            if (feedback == null)
            {
                return HttpNotFound();
            }
            return View(feedback);
        }

        // POST: Feedback/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Feedback feedback = db.Feedbacks.Find(id);
            db.Feedbacks.Remove(feedback);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private BaseInfo GetSiteInfo()
        {
            BaseInfo info = new BaseInfo();
            XDocument site = XDocument.Load(Server.MapPath("~/site.xml"));
            XElement siteInfo = site.Element("site");
            info.Domain = siteInfo.Element("domain").Value;
            info.Name = siteInfo.Element("name").Value;
            info.Logo = siteInfo.Element("logo").Value;
            info.Company = siteInfo.Element("company").Value;
            info.Address = siteInfo.Element("address").Value;
            info.Tel = siteInfo.Element("tel").Value;
            info.Fax = siteInfo.Element("fax").Value;
            info.Email = siteInfo.Element("email").Value;
            info.Crod = siteInfo.Element("crod").Value;
            info.Copyright = siteInfo.Element("copyright").Value;
            info.Kefu = siteInfo.Element("kefu").Value;
            info.CountCode = siteInfo.Element("countCode").Value;
            info.WebClick = siteInfo.Element("webClick").Value;
            info.Seo_title = siteInfo.Element("seoTitle").Value;
            info.Seo_keywords = siteInfo.Element("seoKeywords").Value;
            info.Seo_description = siteInfo.Element("seoDescription").Value;
            return info;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
