﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Enterprise.Models;
using Webdiyer.WebControls.Mvc;
using Microsoft.Security.Application;

namespace Enterprise.Controllers
{
    [Authorize]
    public class ArticleController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Article
        public ActionResult Index(int pageIndex = 1, string Call_index = null)
        {
            var query = db.Articles.AsQueryable();
            if (!string.IsNullOrWhiteSpace(Call_index))
                query = query.Where(x=>x.ArticleCategory.Call_index==Call_index);
            var model = query.OrderByDescending(x => x.Id).ToPagedList(pageIndex, 10);
            return View(model);
        }


        // GET: Article/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // GET: Article/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Article/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Sub_title,Call_index,Img_url,Zhaiyao,Content,Remark,Seo_title,Seo_keyword,Seo_description,Click,Tag,Add_time,Update_time")] Article article, int? categoryId)
        {
            if (ModelState.IsValid)
            {
                if (categoryId != null)
                {
                    article.ArticleCategory = db.ArticleCategories.Find(categoryId);
                }
                if (article.Add_time == null)
                {
                    article.Add_time = DateTime.Now;
                }
                if(article.Click==null)
                {
                    article.Click = 0;
                }
                if(String.IsNullOrEmpty(article.Seo_title))
                {
                    article.Seo_title = article.Title;
                }
                //过滤XSS
                article.Content = Sanitizer.GetSafeHtmlFragment(article.Content);
                article.Author = User.Identity.Name;
                db.Articles.Add(article);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(article);
        }

        // GET: Article/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            if (article.ArticleCategory == null)
            {
                ViewBag.CategoryTitle = "";
            }
            else
            {
                ViewBag.CategoryTitle = article.ArticleCategory.Title;
            }
            return View(article);
        }

        // POST: Article/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Sub_title,Call_index,Img_url,Zhaiyao,Content,Remark,Seo_title,Seo_keyword,Seo_description,Click,Tag,Add_time,Update_time")] Article article, int? categoryId)
        {
            if (ModelState.IsValid)
            {
                Article tempArticle = db.Articles.Find(article.Id);
                if (categoryId != null)
                {
                    tempArticle.ArticleCategory = db.ArticleCategories.Find(categoryId);
                }
                tempArticle.Title = article.Title;
                tempArticle.Sub_title = article.Sub_title;
                tempArticle.Img_url = article.Img_url;
                tempArticle.Zhaiyao = article.Zhaiyao;
                tempArticle.Content = Sanitizer.GetSafeHtmlFragment(article.Content);
                tempArticle.Remark = article.Remark;
                tempArticle.Seo_title = article.Seo_title;
                tempArticle.Seo_keyword = article.Seo_keyword;
                tempArticle.Seo_description = article.Seo_description;
                tempArticle.Add_time = article.Add_time;
                tempArticle.Click = article.Click;
                tempArticle.Tag = article.Tag;
                tempArticle.Call_index = article.Call_index;
                tempArticle.Update_time = DateTime.Now;
                db.Entry(tempArticle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(article);
        }

        // GET: Article/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // POST: Article/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Article article = db.Articles.Find(id);
            db.Articles.Remove(article);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //百度webupload上传控件
        public ActionResult UpLoadProcess(string id, HttpPostedFileBase file)
        {
            string filePathName = string.Empty;

            string localPath = Path.Combine(HttpRuntime.AppDomainAppPath, "Content\\Images");
            if (Request.Files.Count == 0)
            {
                return Json(new { jsonrpc = 2.0, error = new { code = 102, message = "保存失败" }, id = "id" });
            }

            string ex = Path.GetExtension(file.FileName);
            filePathName = Guid.NewGuid().ToString("N") + ex;
            if (!System.IO.Directory.Exists(localPath))
            {
                System.IO.Directory.CreateDirectory(localPath);
            }
            file.SaveAs(Path.Combine(localPath, filePathName));

            return Json(new { jsonrpc = "2.0", id = id, filePath = "/Content/Images/" + filePathName });
        }

        //文章分类
        [ChildActionOnly]
        public ActionResult Category()
        {
            IList<ArticleCategory> categories = db.ArticleCategories.ToList();
            return PartialView("_Category", categories);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

// 
