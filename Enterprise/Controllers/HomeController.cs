﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using Enterprise.Models;
using System.Xml.Linq;

namespace Enterprise.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            HomeModel model = new HomeModel();
            model.BaseInfo = GetSiteInfo();
            //增加网站访问量
            BaseInfo info = new BaseInfo();
            XDocument site = XDocument.Load(Server.MapPath("~/site.xml"));
            XElement siteInfo = site.Element("site");
            siteInfo.Element("webClick").Value = (int.Parse(siteInfo.Element("webClick").Value) + 1).ToString();
            siteInfo.Save(Server.MapPath("~/site.xml"));

            //产品展示
            model.Products = db.Articles.Where(x => x.ArticleCategory.Call_index == "product").Take(4).OrderByDescending(x => x.Add_time);
            //新闻头条
            model.TopNews = db.Articles.Where(x => x.ArticleCategory.Call_index == "news").OrderByDescending(x => x.Add_time).Take(1).FirstOrDefault();
            //新闻 
            model.News = db.Articles.Where(x => x.ArticleCategory.Call_index == "news").OrderByDescending(x => x.Add_time).Skip(1).Take(3);
            //案例展示
            model.Cases = db.Articles.Where(x => x.ArticleCategory.Call_index == "case").OrderByDescending(x => x.Add_time).Take(8);
            //友情链接
            model.FriendLinks = db.FriendLinks.OrderByDescending(x => x.Sort_id);
            //关于我们
            model.AboutUs = db.Articles.Where(x=>x.Call_index=="aboutus").FirstOrDefault();
            //联系我们
            model.ContactUs = db.Articles.Where(x=>x.Call_index=="contactus").FirstOrDefault();
            return View(model);
        }

        public ActionResult Detail(int id)
        {
            DetailModel model = new DetailModel();
            model.BaseInfo = GetSiteInfo();
            model.Article = db.Articles.OrderByDescending(x => x.Id).Where(x => x.Id == id).FirstOrDefault();
            //增加文章访问量
            Article article = db.Articles.Find(id);
            article.Click += 1;
            db.Entry(article).State = EntityState.Modified;
            db.SaveChanges();

            //上一条数据
            model.PreArticle = db.Articles.OrderByDescending(x => x.Id).FirstOrDefault(x => x.Id < id);

            //下一条数据
            model.NextArticle = db.Articles.OrderBy(x => x.Id).FirstOrDefault(x => x.Id > id);

            return View(model);
        }

        public ActionResult List(string callIndex)
        {
            ListModel model = new ListModel();
            model.BaseInfo = GetSiteInfo();
            model.Articles = db.Articles.Where(x => x.ArticleCategory.Call_index == callIndex).OrderByDescending(x => x.Add_time).ToList();
            return View(model);
        }

        public ActionResult ImageList(string callIndex)
        {
            ListModel model = new ListModel();
            model.BaseInfo = GetSiteInfo();
            model.Articles = db.Articles.Where(x => x.ArticleCategory.Call_index == callIndex).OrderByDescending(x => x.Add_time).ToList();
            return View(model);
        }

        public ActionResult RecruitList(string callIndex)
        {
            ListModel model = new ListModel();
            model.BaseInfo = GetSiteInfo();
            model.Articles = db.Articles.Where(x => x.ArticleCategory.Call_index == callIndex).OrderByDescending(x => x.Add_time).ToList();
            return View(model);
        }


        [ChildActionOnly]
        public ActionResult GetProducts(string callIndex)
        {
            return PartialView("_GetProducts", db.Articles.Where(x => x.ArticleCategory.Call_index == callIndex).Take(4).OrderByDescending(x => x.Add_time));
        }

        [ChildActionOnly]
        public ActionResult GetTopNews(string callIndex)
        {
            return PartialView("_GetTopNews", db.Articles.Where(x => x.ArticleCategory.Call_index == callIndex).OrderByDescending(x => x.Add_time).Take(1).ToList());
        }

        [ChildActionOnly]
        public ActionResult GetNews(string callIndex)
        {
            return PartialView("_GetNews", db.Articles.Where(x => x.ArticleCategory.Call_index == callIndex).OrderByDescending(x => x.Add_time).Skip(1).Take(3).ToList());
        }

        [ChildActionOnly]
        public ActionResult GetCase(string callIndex)
        {
            return PartialView("_GetCase", db.Articles.Where(x => x.ArticleCategory.Call_index == callIndex).OrderByDescending(x => x.Add_time).Take(8).ToList());
        }


        [ChildActionOnly]
        public ActionResult GetSlider(string callIndex)
        {
            return PartialView("_GetSlider", db.Articles.Where(x => x.ArticleCategory.Call_index == callIndex).Take(3).OrderByDescending(x => x.Add_time));
        }

        private BaseInfo GetSiteInfo()
        {
            BaseInfo info = new BaseInfo();
            XDocument site = XDocument.Load(Server.MapPath("~/site.xml"));
            XElement siteInfo = site.Element("site");
            info.Domain = siteInfo.Element("domain").Value;
            info.Name = siteInfo.Element("name").Value;
            info.Logo = siteInfo.Element("logo").Value;
            info.Company = siteInfo.Element("company").Value;
            info.Address = siteInfo.Element("address").Value;
            info.Tel = siteInfo.Element("tel").Value;
            info.Fax = siteInfo.Element("fax").Value;
            info.Email = siteInfo.Element("email").Value;
            info.Crod = siteInfo.Element("crod").Value;
            info.Copyright = siteInfo.Element("copyright").Value;
            info.Kefu = siteInfo.Element("kefu").Value;
            info.CountCode = siteInfo.Element("countCode").Value;
            info.WebClick = siteInfo.Element("webClick").Value;
            info.Seo_title = siteInfo.Element("seoTitle").Value;
            info.Seo_keywords = siteInfo.Element("seoKeywords").Value;
            info.Seo_description = siteInfo.Element("seoDescription").Value;
            return info;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}


// 
