﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class GetKey
    {
        public GetKey()
        {
        }

        public static void GetKeyFunction(string username)
        {
            string keyFileName = System.Web.HttpContext.Current.Server.MapPath("~/Key/");
            const int keySize = 1024;

            RSACryptoServiceProvider sp = new RSACryptoServiceProvider(keySize);

            string str = sp.ToXmlString(true);
            string KeyXml = username + "_" + Guid.NewGuid().ToString();
            System.Web.HttpContext.Current.Session["key"] = KeyXml;
            TextWriter writer = new StreamWriter(keyFileName + KeyXml + "_private.xml");
            writer.Write(str);
            writer.Close();

            str = sp.ToXmlString(false);
            writer = new StreamWriter(keyFileName + KeyXml + "_public.xml");
            writer.Write(str);
            writer.Close();
        }
    }
}
